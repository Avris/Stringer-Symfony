<?php

namespace Avris\StringerBundle\Controller;

use Avris\Stringer\Stringer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function demoAction()
    {
        $times = [];
        for ($i = 0; $i < 10; $i++) {$times[] = new \DateTime(rand(0,23).':'.rand(0,59).':'.rand(0,59));}

        /** @var Stringer $stringer */
        $stringer = $this->get('avris.stringer');
        $number = rand(1,3);
        $message = 'Naliczono punkty '.$number. ' użytkownik'.$stringer->decline($number,'owi','om','om').'.';

        return $this->render('AvrisStringerBundle:Default:demo.html.twig', [
            'users' => [
                ['firstName' => 'Andrzej',   'gender' => 'm', 'points' => rand(1,2000)],
                ['firstName' => 'Tomasz',    'gender' => 'm', 'points' => rand(1,2000)],
                ['firstName' => 'Tomek',     'gender' => 'm', 'points' => rand(1,2000)],
                ['firstName' => 'Anna',      'gender' => 'k', 'points' => rand(1,2000)],
                ['firstName' => 'Jan Maria', 'gender' => 'm', 'points' => rand(1,2000)],
                ['firstName' => 'Kornelia',  'gender' => 'k', 'points' => rand(1,2000)],
                ['firstName' => 'Ela',       'gender' => 'k', 'points' => rand(1,2000)],
            ],
            'numbers' => [
                rand(0,1000), rand(-500,-1), rand(-500,500), rand(10000000,10000000),
                rand(1000000,10000000)/1000, rand(10000,100000)/10000, rand(0,1000)/1000000,
            ],
            'time' => new \DateTime(),
            'times' => $times,
            'message' => $message,
            'commentators' => ['A','B','C','D','E','F'],
            'relativeDates' => [
                '-2 years',  '-13 months',    '-5 months',   '-32 days',    '-5 days 8:00', '-2 days 8:00', '-1 day 10:00', '-1 day 23:59:59',
                '-5 hours',  '-175 minutes',  '-69 minutes', '-35 minutes', '-20 minutes',  '-5 minutes',   '-2 minutes',   '-1 minute', 'now',
                '+1 minute', '+2 minutes',    '+5 minutes',  '+20 minutes', '+35 minutes',  '+69 minutes',  '+175 minutes', '+5 hours',
                '+1 day',    '+2 days 15:00', '+10 days',    '+32 days',    '+5 months',    '+13 months',   '+5 years',
            ],
        ]);
    }
}
