## Avris Stringer -- Symfony bridge##

Dokumentacja Stringera dostępna jest pod adresem **[docs.avris.it/stringer](http://docs.avris.it/stringer)**.

Aby zainstalować bridge dla frameworka Symfony2, wykonaj polecenie w konsoli polecenie:

	composer require avris/stringer-symfony

a następnie zarejestruj bundle w pliku `app/AppKernel.php`:

	$bundles = array(
		// ...
		new Avris\StringerBundle\AvrisStringerBundle(),
	);

Jeśli chcesz przyjrzeć się przykładowemu użyciu biblioteki, dodaj do pliku `app/config/routing_dev.yml` linijki:

	avris_stringer:
		resource: "@AvrisStringerBundle/Resources/config/routing.yml"

Teraz przykładowe użycie jest dostępne w przeglądarce pod adresem `/app_dev.php/avris/stringer/demo`.
Kod możesz obejrzeć w pliku `vendor/avris/stringer-symfony/Resources/views/Default/demo.html.twig`.

Wszystkie funkcje biblioteki są dostępne zarówno w widoku jako filtry Twiga,
jak i w innych miejscach kodu -- jako serwis.

    $vocative = $this->get('avris.stringer')->vocative('Michał');
